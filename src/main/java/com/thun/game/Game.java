/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thun.game;

/**
 *
 * @author ASUS
 */
import java.util.*;
public class Game {
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row,col;
    Scanner kb = new Scanner (System.in);
    public Game(){
        playerX = new Player('X');
        playerO = new Player('O');
        table  = new Table(playerX,playerO);
    }
    public void showWelcome(){
        System.out.println("Welcome to OX Game");
    } 
    public void showTable(){
        table.showTable();
    }
    public void input(){
        while(true){
             System.out.println("Please input Row Col:");
            row =kb.nextInt()-1;
            col = kb.nextInt()-1;
            if(table.setRowCol(row,col)){
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }
    public void showTurn(){
        System.out.println(table.getCurrentPlayer().getName() +" turn");
    }
    public void showBye(){
        System.out.println("Bye bye ..."); 
   }
    public void newGame(){
        table = new Table(playerX,playerO);
        table.switchPlayer();
    }
    public void showScore(){
        System.out.println("Total score !!");
        System.out.println("Player X win "+playerX.getWin()+" game");
        System.out.println("Player O win "+playerO.getWin()+" game");
        System.out.println("Draw "+playerX.getDraw()+" game");
    }
    public void run(){
        this.showWelcome();
        while(true){
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if(table.isFinish()){
                if(table.getWinner()==null){
                     System.out.println("Draw!!");
                }
                else{
                    System.out.println(table.getWinner().getName()+" WIn!!");
                }
                this.showTable();
                System.out.println("Do you want to play again?");
                System.out.println("y/n");
                char input = kb.next().charAt(0);
                if(input =='y'){
                    newGame();
                }
                else if(input == 'n'){
                    break;
                }
                else{
                    System.out.println("Please input y or n");
                }
                
            }
            table.switchPlayer();
        }
        this.showScore();
        this.showBye();
        
    }
}
